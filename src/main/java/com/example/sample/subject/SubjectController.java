package com.example.sample.subject;

import com.example.sample.student.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class SubjectController {

    private final SubjectService subjectService;
    private final StudentService studentService;

    @Autowired
    public SubjectController(SubjectService subjectService, StudentService studentService) {
        this.subjectService = subjectService;
        this.studentService = studentService;
    }

    @GetMapping("/subjects") // serves as restful endpoint
    public List<Subject> getAllSubjects() {
        return subjectService.getAllSubjects();
    }

    @GetMapping("/subject/{id}")
    public Subject getSubject(@PathVariable Long id) {
        return subjectService.getSubject(id);
    }

    @PostMapping("/subjects")
    public void addSubject(@RequestBody Subject subject) {
        subjectService.addSubject(subject);
    }

    @PutMapping("/subject/{id}")
    public void addSubject(@PathVariable Long id, @RequestBody Subject subject) {
        subjectService.updateSubject(id, subject);
    }

    @DeleteMapping("/subject/{id}")
    public void deleteSubject(@PathVariable long id){
        subjectService.deleteSubject(id);
    }
}
