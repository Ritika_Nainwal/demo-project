package com.example.sample.student;
import com.example.sample.subject.Subject;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity //FOR HIBERNATE. marker annotation, which is used to discover persistent entities.
@Table //FOR TABLE IN DB. to map this entity to another table (and, optionally, a specific schema)
public class Student {
    //for primary key
//    @SequenceGenerator(
//            name = "student_sequence",
//            sequenceName = "student_sequence",
//            allocationSize =  1
//    )
//    @GeneratedValue(
//            strategy = GenerationType.SEQUENCE,
//            generator = "student_sequence"
//    )
    @Id
    private Long id;
    private String name;
    private Integer studentClass;
    private Integer rollNo;

    @ManyToMany
    @JoinTable(name = "student_subject",
                joinColumns = @JoinColumn(name = "student_id"),
                inverseJoinColumns = @JoinColumn(name = "subject_id"))
    private List<Subject> subjects = new ArrayList<Subject>();

    public Student() {
    }

    public Student(Long id, String name, Integer studentClass, Integer rollNo, List<Subject> subjects) {
        this.id = id;
        this.name = name;
        this.studentClass = studentClass;
        this.rollNo = rollNo;
        this.subjects = subjects;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getStudentClass() {
        return studentClass;
    }

    public void setStudentClass(Integer studentClass) {
        this.studentClass = studentClass;
    }

    public Integer getRollNo() {
        return rollNo;
    }

    public void setRollNo(Integer rollNo) {
        this.rollNo = rollNo;
    }

    public List<Subject> getSubjects() {
        return subjects;
    }

    public void setSubjects(List<Subject> subjects) {
        this.subjects = subjects;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", studentClass=" + studentClass +
                ", rollNo=" + rollNo +
                ", subjects=" + subjects +
                '}';
    }
}
