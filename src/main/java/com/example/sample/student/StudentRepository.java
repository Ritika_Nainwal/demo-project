package com.example.sample.student;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository //This interface is responsible for data access
public interface StudentRepository extends JpaRepository<Student, Long> { //<type this repo will work on, type of pk

}
