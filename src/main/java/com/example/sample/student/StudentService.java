package com.example.sample.student;

import com.example.sample.subject.Subject;
import com.example.sample.subject.SubjectRepository;
import com.example.sample.subject.SubjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

//@Component //Such classes are considered as candidates for auto-detection when using annotation-based configuration and classpath scanning.
@Service //same as component but for better semantics, better readability
public class StudentService{
    @Autowired
    StudentRepository studentRepository;

    SubjectRepository subjectRepository;

    @Autowired
    private SubjectService subjectService;

    public List<Student> getAllStudents(){
        List<Student> students = new java.util.ArrayList<>();
        studentRepository.findAll().forEach(students::add);
        return students;
    }

    public List<Student> searchStudent(String name)
    {
        List<Student> stud = new ArrayList<>();
        studentRepository.findAll().forEach(stud::add);
        return stud.stream().filter(t -> t.getName().contains(name)).collect(Collectors.toList());
    }

//    public Student getStudent(Long id) {
//        Optional<Student> student = studentRepository.findById(id);
//        if (student.isPresent()) {
//            return student.get();
//        } else {
//            return null;
//        }
//    }

    public Student getStudent(Long id) {
//        List<Student> students = getAllStudents();
//        return students.stream().filter(t -> t.getId().equals(id)).findFirst().get();
        return studentRepository.findById(id).orElse(null);
    }

    public void addStudent(Student student){
        studentRepository.save(student);
    }

//    public void addStudent(Student student) {
//        Student newStudent = new Student();
//        newStudent.getSubjects()
//                .addAll(student
//                        .getSubjects()
//                        .stream()
//                        .map(v -> {
//                            Subject vv = subjectService.findSubjectById(v.getId());
//                            vv.getStudents().add(newStudent);
//                            return vv;
//                        }).collect(Collectors.toList()));
//        studentRepository.save(newStudent);
//    }

    public void deleteStudent(Long id){
        studentRepository.deleteById(id);
    }

    public void updateStudent(Long id, Student student) {
        studentRepository.save(student);
    }
}
