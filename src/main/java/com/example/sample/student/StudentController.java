package com.example.sample.student;

import com.example.sample.subject.Subject;
import com.example.sample.subject.SubjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController //The @RestController annotation is used to define the RESTful web services.marks the class as a controller where every method returns a domain object instead of a view.

public class StudentController {

//    @GetMapping
//    public String hello(){
//        return "Hello World";
//    }

    private final StudentService studentService;
    private final SubjectService subjectService;

    @Autowired //enables you to inject the object dependency implicitly.
    public StudentController(StudentService studentService, SubjectService subjectService) {
        this.studentService = studentService;
        this.subjectService = subjectService;
    }

    @GetMapping // serves as restful endpoint
    public List<Student> getAllStudents() {
        return studentService.getAllStudents();
    }

    @GetMapping("/student/{id}")
    public Student getStudent(@PathVariable Long id) {
        return studentService.getStudent(id);
    }

    @PostMapping()
    public void addStudent(@RequestBody Student student) {

        studentService.addStudent(student);
    }

    @PutMapping("/student/{id}")
    public void addStudent(@PathVariable Long id, @RequestBody Student student) {
        studentService.updateStudent(id, student);
    }

    @GetMapping("/students/{name}")
    public List<Student> findBySubstring(@PathVariable String name) {
        return studentService.searchStudent(name);
    }

    @DeleteMapping("/student/{id}")
    public void deleteStudent(@PathVariable long id){
        studentService.deleteStudent(id);
    }
}