//package com.example.sample.student;
//
//import org.springframework.boot.CommandLineRunner;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
//import java.time.LocalDate;
//import java.util.List;
//
//
////@Configuration annotation indicates that a class declares one or more @Bean methods and may
//// be processed by the Spring container to generate bean definitions and service requests
//// for those beans at runtime.
//
//@Configuration
//public class StudentConfig {
//    //    A bean is an object that is instantiated, assembled, and otherwise managed by a Spring IoC container.
//    @Bean
//    CommandLineRunner commandLineRunner(StudentRepository repository) {
//        return args -> {
//            Student abhinav = new Student(
//                    1L,
//                    "Abhinav",
//                    7,
//                    2
//            );
//
//            Student ritika = new Student(
//                    2L,
//                    "Ritika",
//                    10,
//                    28
//            );
//
//            Student parul = new Student(
//                    3L,
//                    "Parul",
//                    12,
//                    24
//            );
//
//            //hibernate runs when we invoke saveAll
//            repository.saveAll(
//                    List.of(abhinav, ritika, parul)
//            );
//        };
//    }
//
//}